/*
Copyright © 2022 Shane Moore shane@shanemoore.me

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"os"
	"os/exec"
	"path/filepath"

	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/smoores/open-devcontainer/containers"
)

var attachCmd = &cobra.Command{
	Use:   "attach",
	Short: "Attach a tty to a running devcontainer",
	Long: `Run an interactive shell in a running devcontainer.
This command will fail if a devcontainer hasn't already been
started for this workspace. See 'odc run' for more information
about starting new devcontainers.

By default, /bin/bash is used as the interactive shell. To choose
a different shell, use the '--shell' flag to pass a path to a
different shell executable.`,
	Run: func(cmd *cobra.Command, args []string) {
		manifestPath, _ := filepath.Abs(relativeManifestPath)
		manifest, err := readManifest(manifestPath)
		if err != nil {
			return
		}

		runtime := viper.GetString("runtime")

		containerConfig, err := containers.GetContainerConfig(manifest.ContainerName())
		if err != nil || !containerConfig.State.Running {
      log.Error().Msg("Container isn't running. Try `odc run` to start it.")
			return
		}

		sublimeTextCompat := containerConfig.Config.Labels != nil && containerConfig.Config.Labels["sublime-text-compat"] == "true"
		workingDir := manifest.WorkingDir(sublimeTextCompat)

		shell := viper.GetString("shell")
		execArgs := []string{"container", "exec", "--interactive", "--tty", "--workdir", workingDir}
		if manifest.ContainerExecUser() != "" {
			execArgs = append(execArgs, "--user", manifest.ContainerExecUser())
		}
		for _, envVar := range manifest.RemoteEnvSlice() {
			execArgs = append(execArgs, "--env", envVar)
		}
		execArgs = append(execArgs, manifest.ContainerName())
		execArgs = append(execArgs, shell)
		execCmd := exec.Command(runtime, execArgs...)
		execCmd.Stdout = os.Stdout
		execCmd.Stderr = os.Stderr
		execCmd.Stdin = os.Stdin

		err = execCmd.Start()
		if err != nil {
      log.Error().Err(err).Msg("Could not create exec process")
			return
		}
		defer execCmd.Wait()
	},
}

func init() {
	rootCmd.AddCommand(attachCmd)

	attachCmd.Flags().StringP("shell", "s", "/bin/bash", "The shell to use in the attached container")
	viper.BindPFlag("shell", attachCmd.Flags().Lookup("shell"))
}
