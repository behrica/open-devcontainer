package main

import (
	"fmt"
	"io"
	"net"
	"os"
	"os/signal"
	"syscall"

	flag "github.com/spf13/pflag"
)

var network string
var address string

func main() {
	flag.StringVarP(&network, "network", "n", "tcp", "Known networks are \"tcp\", \"tcp4\" (IPv4-only), \"tcp6\" (IPv6-only), \"udp\", \"udp4\" (IPv4-only), \"udp6\" (IPv6-only), \"ip\", \"ip4\" (IPv4-only), \"ip6\" (IPv6-only), \"unix\", \"unixgram\" and \"unixpacket\"")
	flag.StringVarP(&address, "address", "a", "", "Address to forward connection to")
	flag.String("uuid", "", "Unique ID to identify this connection")
	flag.Parse()

	connection, err := net.Dial(network, address)
	if err != nil {
		fmt.Println("Couldn't establish connection to "+address+":", err)
	}
	defer connection.Close()

	go io.Copy(os.Stdout, connection)
	go io.Copy(connection, os.Stdin)

	done := make(chan os.Signal, 1)
	signal.Notify(done, syscall.SIGINT, syscall.SIGTERM)
	<-done
}
